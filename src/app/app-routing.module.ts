import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { LogInComponent } from './login/log-in/log-in.component';
import { DisplayComponent } from './display/display.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
  {path:'register', component:RegistrationComponent},
  {path:'login', component:LogInComponent},
  {path:'display', component:DisplayComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full' },
  {path:'resetpassword', component:ResetPasswordComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
