import { Component, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router'
import { ErrorDialogComponent } from 'src/app/dialogs/error-dialog/error-dialog.component';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent {

  logInForm = new FormGroup({username: new FormControl('', Validators.required),
                              password: new FormControl('', Validators.required)})
  constructor( private router: Router,
              private registrationService: RegistrationService,
              // private dialog: MatDialog
              ) {}
              
  register(logInForm:FormGroup) {
    if (logInForm.value !== undefined && logInForm.valid) {
      this.registrationService.authenticateUser(logInForm.value).subscribe(
        (res) => {
          logInForm.reset();
          this.router.navigate(['display'])

        },
        (error) => {
          console.log(error);
          // this.modal.open()
        }
      )
    }
  }
  signUp() {
    this.router.navigate(['register']);
  }
  resetPassword() {
    this.router.navigate(['resetpassword']);
  }
  

}
