import { ChangeDetectorRef, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent {
    resetSuccessfull: boolean = false;

    resetPassword = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('',Validators.required),
    confpaswd: new FormControl('', Validators.required)
  })
  constructor(private registrationService: RegistrationService,
    private router: Router,
    private cdref: ChangeDetectorRef) {}

  reset(resetPassword: FormGroup){
    if (resetPassword !== undefined && resetPassword.value) {
      this.registrationService.reset_password(resetPassword.value).subscribe(
        (res) => {
          resetPassword.reset();
          // this.router.navigate(['login']);
          this.resetSuccessfull = true;
          this.cdref.detectChanges();
        },
        (error) => {
          console.log(error);
        }
      )
      
    }
  }

  login() {
    this.router.navigate(['login'])
  }
}
